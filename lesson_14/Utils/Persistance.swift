//
//  Persistance.swift
//  lesson_14
//
//  Created by Oleksandr Karpenko on 03.08.2020.
//  Copyright © 2020 Oleksandr Karpenko. All rights reserved.
//

import Foundation
import RealmSwift
import CoreData

class Persistence {
    static let shared = Persistence()
    
    private let kFirstNameKey = "Persistance.kFirstNameKey"
    var firstName: String? {
        get { return UserDefaults.standard.string(forKey: kFirstNameKey) }
        set { UserDefaults.standard.set(newValue, forKey: kFirstNameKey)}
    }
    
    private let kLastNameKey = "Persistance.kLastNameKey"
    var lastName: String? {
        get { return UserDefaults.standard.string(forKey: kLastNameKey) }
        set { UserDefaults.standard.set(newValue, forKey: kLastNameKey)}
    }
    
    private let kWeather = "Persistance.kWeather"
    var weather: String? {
        get { return UserDefaults.standard.string(forKey: kWeather) }
        set { UserDefaults.standard.set(newValue, forKey: kWeather)}
    }
    
    private let realm = try! Realm()
    
    func addTodo(_ todo: TodoEntity) {
        try! realm.write {
            realm.add(todo)
        }
    }
    
    func removeTodo(_ todo: TodoEntity) {
        try! realm.write {
            realm.delete(todo)
        }
    }
    
    func getTodos() -> [TodoEntity] {
        return Array(realm.objects(TodoEntity.self))
    }
    
    func addTodoV2(_ todo: TodoEntity) -> NSManagedObject? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return nil
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Todo", in: managedContext)!
        let newTodo = NSManagedObject(entity: entity, insertInto: managedContext)
        
        newTodo.setValue(todo.todo, forKeyPath: "todo")
        
        do {
            try managedContext.save()
            return newTodo
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        return nil
    }
    
    func removeTodoV2(_ todo: NSManagedObject) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Todo")

        request.predicate = NSPredicate(format:"todo = %@", todo.value(forKey: "todo") as! String)

        let result = try? managedContext.fetch(request)
        let resultData = result as! [NSManagedObject]
        
        for object in resultData {
            managedContext.delete(object)
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
}
