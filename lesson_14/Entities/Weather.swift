//
//  Weather.swift
//  lesson_14
//
//  Created by Oleksandr Karpenko on 04.08.2020.
//  Copyright © 2020 Oleksandr Karpenko. All rights reserved.
//

import UIKit

class Weather {
    
    var type: String
    var description: String
    var temp: Double

    init?(json: NSDictionary) {
        guard
            let weather = json["weather"] as? [NSDictionary],
            let type = weather[0]["main"] as? String,
            let description = weather[0]["description"] as? String,
            let main = json["main"] as? NSDictionary,
            let temp = main["temp"] as? Double
            else { return nil }
        
        self.type = type
        self.description = description
        self.temp = temp
    }
    
    init?(json: [String: Any]) {
        guard
            let weather = json["weather"] as? [NSDictionary],
            let type = weather[0]["main"] as? String,
            let description = weather[0]["description"] as? String,
            let main = json["main"] as? NSDictionary,
            let temp = main["temp"] as? Double
            else { return nil }
        
        self.type = type
        self.description = description
        self.temp = temp
    }
    
    static func getArray(from jsonArray: Any) -> [Weather]? {
        guard let jsonArray = jsonArray as? Array<NSDictionary> else { return nil }
        return jsonArray.compactMap { Weather(json: $0) }
    }
}
