//
//  Todo.swift
//  lesson_14
//
//  Created by Oleksandr Karpenko on 03.08.2020.
//  Copyright © 2020 Oleksandr Karpenko. All rights reserved.
//

import Foundation
import RealmSwift

class TodoEntity: Object {
    @objc dynamic var todo = ""
}
