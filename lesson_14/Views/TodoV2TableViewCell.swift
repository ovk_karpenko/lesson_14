//
//  TodoV2TableViewCell.swift
//  lesson_14
//
//  Created by Oleksandr Karpenko on 04.08.2020.
//  Copyright © 2020 Oleksandr Karpenko. All rights reserved.
//

import UIKit

class TodoV2TableViewCell: UITableViewCell {

    var removeTodoAction: (() -> Void)?
    
    @IBOutlet weak var todoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func removeTodo(_ sender: Any) {
        if let action = removeTodoAction {
            action()
        }
    }
    
}
