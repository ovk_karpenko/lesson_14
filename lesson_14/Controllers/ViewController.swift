//
//  ViewController.swift
//  lesson_14
//
//  Created by Oleksandr Karpenko on 03.08.2020.
//  Copyright © 2020 Oleksandr Karpenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstNameTextField.text = Persistence.shared.firstName
        lastNameTextField.text = Persistence.shared.lastName
    }

    @IBAction func firstNameTextFieldEditing(_ sender: Any) {
        Persistence.shared.firstName = firstNameTextField.text
    }

    @IBAction func lastNameTextFieldEditing(_ sender: Any) {
        Persistence.shared.lastName = lastNameTextField.text
    }
}

