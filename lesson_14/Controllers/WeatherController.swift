//
//  WeatherController.swift
//  lesson_14
//
//  Created by Oleksandr Karpenko on 04.08.2020.
//  Copyright © 2020 Oleksandr Karpenko. All rights reserved.
//

import UIKit

class WeatherController: UIViewController {
    
    @IBOutlet weak var wetherLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        wetherLabel.text = Persistence.shared.weather
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let apiKey = "99dbde551d2c1db4f0d154db4f89ba3c"
        let zipCode = "103274"
        
        OpenweathermapApiV2(apiKey).getCurrentWeather(zipcode: zipCode, complation: { weather in
            let response = "Temp: \(weather.temp)\nType: \(weather.type)\nDescription: \(weather.description)"
            
            Persistence.shared.weather = response
            self.wetherLabel.text = response
        })
    }
    
}
