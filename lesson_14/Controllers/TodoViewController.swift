//
//  TodoViewController.swift
//  lesson_14
//
//  Created by Oleksandr Karpenko on 03.08.2020.
//  Copyright © 2020 Oleksandr Karpenko. All rights reserved.
//

import UIKit

class TodoViewController: UIViewController {
    
    var todos = Persistence.shared.getTodos()
    
    @IBOutlet weak var todoTextField: UITextField!
    @IBOutlet weak var todoTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func addTodo(_ sender: Any) {
        if let todo = todoTextField.text, !(todo.isEmpty) {
            let newTodo = TodoEntity()
            newTodo.todo = todo
            
            Persistence.shared.addTodo(newTodo)
            todos.append(newTodo)
            todoTable.reloadData()
        }
    }
    
}

extension TodoViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let todo = todos[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell") as! TodoTableViewCell
        
        cell.todoLabel.text = todo.todo
        cell.removeTodoAction = {
            self.todos.remove(at: indexPath.row)
            Persistence.shared.removeTodo(todo)
            tableView.reloadData()
        }
        
        return cell
    }
    
}
