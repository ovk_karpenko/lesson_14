//
//  TodoV2ViewController.swift
//  lesson_14
//
//  Created by Oleksandr Karpenko on 04.08.2020.
//  Copyright © 2020 Oleksandr Karpenko. All rights reserved.
//

import UIKit
import CoreData

class TodoV2ViewController: UIViewController {
    
    var todos: [NSManagedObject] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Todo")
        
        do {
          todos = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
          print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    @IBAction func addTodo(_ sender: Any) {
        let alert = UIAlertController(title: "New Todo",
                                      message: "Add a new task",
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default) {
            [unowned self] action in
            
            guard let textField = alert.textFields?.first,
                let todoToSave = textField.text else {
                    return
            }
            
            let todo = TodoEntity()
            todo.todo = todoToSave
            
            if let addedTodo = Persistence.shared.addTodoV2(todo) {
                self.todos.append(addedTodo)
                self.tableView.reloadData()
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel)
        
        alert.addTextField()
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
    }
    
}

extension TodoV2ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let todo = todos[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell") as! TodoV2TableViewCell
        
        cell.todoLabel?.text = todo.value(forKey: "todo") as? String
        cell.removeTodoAction = {
            Persistence.shared.removeTodoV2(todo)
            
            self.todos.remove(at: indexPath.row)
            tableView.reloadData()
        }
        
        return cell
    }
    
}
