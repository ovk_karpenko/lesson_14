//
//  OpenweathermapApiV2.swift
//  lesson_14
//
//  Created by Oleksandr Karpenko on 04.08.2020.
//  Copyright © 2020 Oleksandr Karpenko. All rights reserved.
//

import UIKit
import Alamofire

class OpenweathermapApiV2 {
    private let apiKey: String
    private let host = "https://api.openweathermap.org/data/2.5"
    
    init(_ apiKey: String) {
        self.apiKey = apiKey
    }
    
    func getCurrentWeather(zipcode: String, complation: @escaping (Weather) -> Void) {
        AF.request("\(host)/weather?q=\(zipcode)&appid=\(apiKey)").responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let value):
                if let json = value as? [String: Any],
                    let weather = Weather(json: json) {
                    complation(weather)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getWeatherForWeek(zipcode: String, complation: @escaping ([Weather]) -> Void) {
        AF.request("\(host)/forecast?q=\(zipcode)&appid=\(apiKey)&cnt=7").responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let value):
                if let json = value as? [String: Any],
                    let list = json["list"] as? [[String: Any]],
                    let weather = Weather.getArray(from: list) {
                    complation(weather)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
